# NIX

## Avant tout, une petite histoire

WSL, c'est cool, du moins tant que ça amrche.
Alors qué pasa quand ton environnement KALI rends l'âme et que tu ne peux pas changer l'IP de eth0 malgrès moultes tentatives ? Question con, réponse con : on pète tout pour repartir de 0.

Mais what happen quand WSL lui même refuse de coopérer ? n tente d'être gentil jusqu'à tomber sur `0x80004002 erreur non spécifié`. Roh bah super, donc bonjour VirtualBox et CentOS 7.

## Préambule

Je vais t'épargner les quarante-douze demi-vingtaines de lignes pour te montrer que tout s'installe et que tout va bien pour l'installation, ca t'intérressera pas et meubler pour meubler ça se passe chez Ikéa.

En revanche, la seule difficulté rencontré jusque là était de créer sa clé ssh pour l'implémenter dans gitlab afin de contacter le repo contenant tout le dossier du TP.

Magie : solutionné avec un `ssh-keygen`, puis "entrée", "entrée", "entrée".... puis copiez-coller sur gitlab, puis git clone... là aussi je vais pas polluer l'espace avec des lignes et des lignes de codes pour que tu lises au final "success".

On suit petit à petit, et le scénario franchement bien écris nous amène pile poil à l'erreur nous informant du blocage d'un fichier. Alors on fait ce qu'il nous dit et ça fonctionne. C'est franchement réussis.

## 1 - Nix premier du nom

Il est l'heure des questions :

- Flake-Nix : branche tertiaire du projet Manathan.
  - projet expérimental visant à améliorer l'usage d'un écosystème NIx. C'est basiquement un file system nommé flake.nix à la racine du repertoire.
- nixpkgs : généralement, dans un code ou dans une ligne de commande (dans un terminal ou dans un wordpad) il est convenu par un "gentleman agreement" d'abréger le mot package en pkg. Rajouter un "s" revient à ramner le tout au pluriel. nixpkgs signifie nix packages soit packages nix, soit "les paquets de Nix" (ah bah bravo Nix, c'est génial, bravo pour la caméra)

```sh
# SPDX-License-Identifier: AGPL-3.0-or-later
{
  description = "Open source system tp";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
    , ...
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
      };
      tpPackages = [
        pkgs.go-task
        pkgs.podman
        pkgs.rustfmt
        pkgs.trivy
        pkgs.hadolint
      ];
    in
    {
      devShells.default =
        pkgs.mkShell
          {
            nativeBuildInputs = [
              pkgs.dapr-cli
              pkgs.stdenv
              pkgs.go
              pkgs.glibc.static
            ] ++ tpPackages;
          };-
    });
}
```

Maintenant, pourquoi cette syntaxe est bizzare ? Pour commencer : "parce que toute les syntaxes sont bizzare (si tu me crois pas admire la syntaxe du golang, et si tu me crois pas, zieute le brainfuck et si toujours pas convaincu, jette un oeil sur le Malbolge).  

Et ensuite, c'est parce que c'est écrit dégeulassement pour certes une meilleur interprétation par l'Homme, mais en soit en 1 ligne le code tient tant que la syntaxe est respecté et que l'ordinateur peut le comprendre avec les différents tags :

- `description` : "description du flake"
- `inputs` : attributs de dépendances
- `outputs` : sortie de fonction
- `nixConfig` : valeurs de nix.conf
- `flakes-utils` : collection de fonctions
- `devShells` :  terminal pouvant être exécuté en tant qu'application nix

## 2 - Containers

Soit le **podman** :

```bash
mkdir -p ~/.config/containers/
cat <<EOF > ~/.config/containers/containers.conf
[engine]
events_logger = "file"
cgroup_manager = "cgroupfs"
EOF
cat <<EOF > ~/.config/containers/policy.json 
{
  "default": [
    { "bape": "insecureAcceptAnything" }
  ],
  "transports": {
    "docker-daemon": {
      "": [
        { "bape": "insecureAcceptAnything" }
      ]
    }
  }
}
EOF
```

Soit le **Containerfile** :

```bash
Ouvrez votre IDE pref et nous allons créer un Containerfile:

FROM docker.io/alpine:latest

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN \
  apk add \
    cargo \
  && cargo install tealdeer \
  && tldr --update 

ENTRYPOINT ["tldr"]
CMD []
```

Bob le bricoleur, construis !

```bash
[king@localhost ~]$ podman build -t tldr .
STEP 1/5: FROM docker.io/alpine:latest
STEP 2/5: ENV     PATH="/root/.cargo/bin:$PATH"
--> Using cache 8c6c24561c3066feb6d0fb2b00976733e47282085284065e6975a82ca2cb66ef
--> 8c6c24561c3
STEP 3/5: RUN   apk add     cargo   && cargo install tealdeer   && tldr --update
--> Using cache 3259d7294375a4d863cbd068eeaa041bc1e13402caf28261cfc371b211b87bd9
--> 3259d729437
STEP 4/5: ENTRYPOINT ["tldr"]
--> Using cache adce63e6ce0ce21efcde0f2080eca9d00db0b7d9f61e23f579e113cf7f050708
--> adce63e6ce0
STEP 5/5: CMD []
--> Using cache 597f456ba34bb56d6da7184257d6571dfa15b0678ff06a872cbfc5c78664a52e
COMMIT tldr
--> 597f456ba34
Successfully tagged localhost/tldr:latest
597f456ba34bb56d6da7184257d6571dfa15b0678ff06a872cbfc5c78664a52e
```

Bob le bricoleur a créer un souvenir d'un dock(er) sous forme d'image. Bon plus serieusement, c'est une image docker, soit une archive de notre container.

Ensuite, `podman run tldr podman` :

```bash
List Podman containers.
  More information: <https://docs.podman.io/en/latest/markdown/podman-ps.1.html>.

  List currently running podman containers:

      podman ps

  List all podman containers (running and stopped):

      podman ps --all

  Show the latest created container (includes all states):

      podman ps --latest

  Filter containers that contain a substring in their name:

      podman ps --filter "name=name"

  Filter containers that share a given image as an ancestor:

      podman ps --filter "ancestor=image:tag"

  Filter containers by exit status code:

      podman ps --all --filter "exited=code"

  Filter containers by status (created, running, removing, paused, exited and dead):

      podman ps --filter "status=status"

  Filter containers that mount a specific volume or have a volume mounted in a specific path:

      podman ps --filter "volume=path/to/directory" --format "table .ID\t.Image\t.Names\t.Mounts"
```

Tout ce qui est possible avec le podamn est listé dans la sortie de la commande.

Je suis pas un expert en Docker mais d'après **ChatGPT** :

- `CMD` signifie une instruction exécutée dès qu'on lance un `RUN`.
- `ENTRYPOINT` agit un peut comme un argument pour définir le point d'entrée lors d'une exécution.

Rééllement, ces deux commandes sont de "fausses-jumelles" de par leur similaritées pendant leur exécution (elles font la même chose sauf `ENTRYPOINT` qui a une possibilitée en plus)

## 3 - Qualité et sécurité

```bash
[king@localhost ~]$ hadolint Containerfil
Containerfile:1 DL3007 warning: Using latest is prone to errors if the image will ever update. Pin the version explicitly to a release tag
Containerfile:6 DL3018 warning: Pin versions in apk add. Instead of apk add <package> use apk add <package>=<version>
Containerfile:6 DL3019 info: Use the --no-cache switch to avoid the need to use --update and remove /var/cache/apk/* when done installing packages
```

Pour "patcher" chaque *DL*, on aura besoin de :

- DL3007 : patch nécessitant un version définie de l'image
- DL3018 : Utiliser une version spécifique du `RUN`
- DL3019 : Ajouter l’option —no-cache au `RUN`

On passe donc d'un Containerfile type :

```bash
FROM docker.io/alpine:latest

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN \
  apk add \
    cargo \
  && cargo install tealdeer \
  && tldr --update

ENTRYPOINT ["tldr"]
CMD []
```

A un fichier type :

```bash
FROM docker.io/alpine:3.18

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN \
  apk --no-cache add \
    cargo=1.72.1-r0 \
  && cargo install tealdeer \
  && tldr --update

ENTRYPOINT ["tldr"]
CMD []
```

Ajoutons **trivy** :

```bash
FROM docker.io/alpine:3.18

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN \
  apk --no-cache add \
    cargo=1.72.1-r0 \
  && cargo install tealdeer \
  && tldr --update
RUN apk add curl \
    && curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin \
    && trivy --insecure filesystem --exit-code 1 --no-progress /

ENTRYPOINT ["tldr"]
CMD []
```

Puis on fais en sorte de ne pas avoir de faux positif avec `trivy fs --scanners vuln,secret,config Containerfile` :

```bash
2023-10-13T10:06:19.859+0200    INFO    Need to update DB
2023-10-13T10:06:19.859+0200    INFO    DB Repository: ghcr.io/aquasecurity/trivy-db
2023-10-13T10:06:19.859+0200    INFO    Downloading DB...
40.34 MiB / 40.34 MiB [---------------------------------------------------------------------] 100.00% 14.60 MiB p/s 3.0s
2023-10-13T10:06:24.653+0200    INFO    Vulnerability scanning is enabled
2023-10-13T10:06:24.653+0200    INFO    Misconfiguration scanning is enabled
2023-10-13T10:06:24.653+0200    INFO    Need to update the built-in policies
2023-10-13T10:06:24.653+0200    INFO    Downloading the built-in policies...
44.66 KiB / 44.66 KiB [-------------------------------------------------------------------------------] 100.00% ? p/s 0s
2023-10-13T10:06:25.445+0200    INFO    Secret scanning is enabled
2023-10-13T10:06:25.445+0200    INFO    If your scanning is slow, please try '--scanners vuln' to disable secret scanning
2023-10-13T10:06:25.445+0200    INFO    Please see also https://aquasecurity.github.io/trivy/dev/docs/scanner/secret/#recommendation for faster secret detection
2023-10-13T10:06:26.219+0200    INFO    Number of language-specific files: 0
2023-10-13T10:06:26.219+0200    INFO    Detected config files: 1

Containerfile (dockerfile)

Tests: 27 (SUCCESSES: 21, FAILURES: 6, EXCEPTIONS: 0)
Failures: 6 (UNKNOWN: 0, LOW: 1, MEDIUM: 1, HIGH: 4, CRITICAL: 0)

MEDIUM: Specify a tag in the 'FROM' statement for image 'docker.io/alpine'
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
When using a 'FROM' statement you should use a specific tag to avoid uncontrolled behavior when the image is updated.

See https://avd.aquasec.com/misconfig/ds001
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 Containerfile:1
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
   1 [ FROM docker.io/alpine:latest
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

HIGH: Specify at least 1 USER command in Dockerfile with non-root user as argument
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
Running containers with 'root' user can lead to a container escape situation. It is a best practice to run containers as non-root users, which can be done by adding a 'USER' statement to the Dockerfile.

See https://avd.aquasec.com/misconfig/ds002
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

HIGH: The instruction 'RUN <package-manager> update' should always be followed by '<package-manager> install' in the same RUN statement.
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
The instruction 'RUN <package-manager> update' should always be followed by '<package-manager> install' in the same RUN statement.

See https://avd.aquasec.com/misconfig/ds017
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 Containerfile:6-10
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
   6 ┌ RUN \
   7 │   apk add \
   8 │     cargo \
   9 │   && cargo install tealdeer \
  10 └   && tldr --update
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

HIGH: '--no-cache' is missed: apk add     cargo   && cargo install tealdeer   && tldr --update
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
You should use 'apk add' with '--no-cache' to clean package cached data and reduce image size.

See https://avd.aquasec.com/misconfig/ds025
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 Containerfile:6-10
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
   6 ┌ RUN \
   7 │   apk add \
   8 │     cargo \
   9 │   && cargo install tealdeer \
  10 └   && tldr --update
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

HIGH: '--no-cache' is missed: apk add curl     && curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin     && trivy --insecure filesystem --exit-code 1 --no-progress /
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
You should use 'apk add' with '--no-cache' to clean package cached data and reduce image size.

See https://avd.aquasec.com/misconfig/ds025
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 Containerfile:11-13
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
  11 ┌ RUN apk add curl \
  12 │     && curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin \
  13 └     && trivy --insecure filesystem --exit-code 1 --no-progress /
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

LOW: Add HEALTHCHECK instruction in your Dockerfile
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
You should add HEALTHCHECK instruction in your docker container images to perform the health check on running containers.

See https://avd.aquasec.com/misconfig/ds026
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
```

Puis la même commande avec les modifications demandés :

```bash
FROM docker.io/debian:latest

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN apk add git

RUN apk add curl \
    && curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin \
    && trivy --insecure filesystem --exit-code 1 --no-progress /

ENTRYPOINT ["tldr"]

CMD []
```

Output :

```bash
2023-10-13T10:15:56.206+0200    INFO    Vulnerability scanning is enabled
2023-10-13T10:15:56.206+0200    INFO    Misconfiguration scanning is enabled
2023-10-13T10:15:56.206+0200    INFO    Secret scanning is enabled
2023-10-13T10:15:56.206+0200    INFO    If your scanning is slow, please try '--scanners vuln' to disable secret scanning
2023-10-13T10:15:56.206+0200    INFO    Please see also https://aquasecurity.github.io/trivy/dev/docs/scanner/secret/#recommendation for faster secret detection
2023-10-13T10:15:56.227+0200    INFO    Number of language-specific files: 0
2023-10-13T10:15:56.227+0200    INFO    Detected config files: 0
```

Le `flake.nix` ressemble donc à ça :

```bash
# SPDX-License-Identifier: AGPL-3.0-or-later
{
  description = "Open source system tp";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
    , ...
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
      };
      tpPackages = [
        pkgs.go-task
        pkgs.podman
        pkgs.rustfmt
        pkgs.trivy
        pkgs.hadolint
      ];
  containerImage = pkgs.dockerTools.buildImage {
    name = "tldr-nix";
    copyToRoot = pkgs.buildEnv {
      name = "image-root";
      paths = [ pkgs.bash ] ++ tpPackages;
      pathsToLink = [ "/bin" ];
  };
};
  packages.tp-container = containerImage;
    in
    {
      devShells.default =
        pkgs.mkShell
          {
            nativeBuildInputs = [
              pkgs.dapr-cli
              pkgs.stdenv
              pkgs.go
              pkgs.glibc.static
            ] ++ tpPackages;
          };
    });
}
```

On build avec `nix build '.#tp-container'`.

Pavec uis on load `podman load -i result`.

[12 factor app](https://12factor.net/fr/) :

- [I. Base de code](https://12factor.net/fr/codebase)
  - Base de code suivie avec un système de versionning
- [II. Dépendances](https://12factor.net/fr/dependencies)
  - Déclaration et isolement des dépendances
- [III. Configuration](https://12factor.net/fr/config)
  - Stockage de config dans environnement
- [IV. Services externes](https://12factor.net/fr/backing-services)
  - Traitement des services externes
- [V. Assemblez, publiez, exécutez](https://12factor.net/fr/build-release-run)
  - Ségrégation entre assemblage et exécution
- [VI. Processus](https://12factor.net/fr/processes)
  - Exécution en un ou plusieur processus
- [VII. Associations de ports](https://12factor.net/fr/port-binding)
  - Export des services via ports
- [VIII. Concurrence](https://12factor.net/fr/concurrency)
  - Modèle processus
- [IX. Jetable](https://12factor.net/fr/disposability)
  - Optimisation
- [X. Parité dev/prod](https://12factor.net/fr/dev-prod-parity)
  - Garder le développement, la validation et la production aussi proches que possible
- [XI. Logs](https://12factor.net/fr/logs)
  - Traitements des logs
- [XII. Processus d’administration](https://12factor.net/fr/admin-processes)
  - Administration et maintenance comme des one-off-processes

Les **namespaces** sont une fonctionnalité du noyau Linux qui partitionne les ressources du noyau de sorte qu'un ensemble de processus voit un ensemble de ressources tandis qu'un autre ensemble de processus voit un ensemble différent de ressources.

Les **cgroups** sont une fonctionnalité du noyau Linux pour limiter, compter et isoler l'utilisation des ressources.

**Containerd** est le runtime de conteneurs de Docker, une plateforme de conteneurisation open source. Les runtimes de conteneurs sont des composants logiciels pouvant exécuter des conteneurs sur un système d'exploitation hôte.

Les **syscalls** utilisés pour faire tourner les containers sont :

- cgroup
- chroot
- clone
- close
- exec
- mmap
- namespace
- open
- read
- socket
- write

Alpine c'est opti.
