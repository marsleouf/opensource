# SDLC ou comment ne pas écrire un TP

## 1 - Before beginning

- bien sachoir cloner un TP
- bien comprendre comment qu'utiliser Git
- bien installer direnv

Ensuite : création de `0001-fix-patch.patch`, then `git apply -v`.

```sh
[root@localhost nuclei]# git apply -v 0001-fix-patch.patch
Checking patch v2/internal/installer/versioncheck.go...
Checking patch v2/internal/runner/runner_test.go...
Checking patch v2/pkg/model/worflow_loader.go => v2/pkg/model/workflow_loader.go...
Checking patch v2/pkg/protocols/headless/engine/page_actions_test.go...
Applied patch v2/internal/installer/versioncheck.go cleanly.
Applied patch v2/internal/runner/runner_test.go cleanly.
Applied patch v2/pkg/model/worflow_loader.go => v2/pkg/model/workflow_loader.go cleanly.
Applied patch v2/pkg/protocols/headless/engine/page_actions_test.go cleanly.
```

## 2 - Environnement

then create .direnvrc (et ca marchera pas parce que bouge pas tu vas voir).

```sh
drwxrwxr-x. 10 king king  4096 Oct 19 12:06 .
drwx------.  9 king king   202 Oct 19 11:11 ..
-rw-r--r--.  1 root root 32609 Oct 19 11:47 0001-fix-patch.patch
-rw-r--r--.  1 root root  1319 Oct 19 11:14 CONTRIBUTING.md
-rw-r--r--.  1 root root  1846 Oct 19 11:14 DEBUG.md
-rw-r--r--.  1 root root 25208 Oct 19 11:14 DESIGN.md
-rw-r--r--.  1 root root   330 Oct 19 11:57 .direnvrc
-rw-r--r--.  1 root root   356 Oct 19 11:14 Dockerfile
drwxr-xr-x.  9 root root   162 Oct 19 11:14 docs
drwxr-xr-x.  8 root root   163 Oct 19 11:15 .git
drwxr-xr-x.  4 root root   135 Oct 19 11:14 .github
-rw-r--r--.  1 root root   561 Oct 19 11:14 .gitignore
drwxr-xr-x.  3 root root    60 Oct 19 11:14 helm
drwxr-xr-x. 17 root root  4096 Oct 19 11:14 integration_tests
-rw-r--r--.  1 root root  1079 Oct 19 11:14 LICENSE.md
-rw-r--r--.  1 root root 52682 Oct 19 11:14 nuclei-jsonschema.json
-rw-r--r--.  1 root root 24690 Oct 19 11:14 README_CN.md
-rw-r--r--.  1 root root 27665 Oct 19 11:14 README_ID.md
-rw-r--r--.  1 root root 22832 Oct 19 11:14 README_KR.md
-rw-r--r--.  1 root root 26689 Oct 19 11:14 README.md
drwxr-xr-x.  2 root root   124 Oct 19 11:14 .run
drwxr-xr-x.  2 root root   204 Oct 19 11:14 static
-rwxr-xr-x.  1 root root 47931 Oct 19 11:14 SYNTAX-REFERENCE.md
-rw-r--r--.  1 root root  1206 Oct 19 11:14 THANKS.md
drwxr-xr-x.  6 root root   146 Oct 19 11:14 v2

```

execution de allow :

```py
[root@localhost nuclei]# direnv allow
direnv: error .envrc file not found
```

oh bah tiens comme par hasard...
Et avant que tu me dises "ouiiiiiii mais faut préciser le nom du fichier pour qu'il execute :

```py
[root@localhost nuclei]# direnv allow ./.direnvrc
direnv: error .envrc file not found
```

faut appeller le fichier .envrc et non .direnvrc sinon le linux il trouve pas.

```sh
[root@localhost nuclei]# mv ./.direnvrc ./.envrc
[root@localhost nuclei]# direnv allow
[root@localhost nuclei]#
```

Mais... Mais... cela fonctionnerait-il ? bah bien évidemment....

___________________

Question: Quel est l'utilité du flake package nix-direnv par rapport à direnv ?

Réponse : qu'est ce que j'en sais moi ? Mais d'après google, c'est plus rapide et persistent que direnv. Google a toujours raison.
___________________

```sh
[king@localhost nuclei]$ direnv allow
warning: Git tree '/home/king/nuclei' is dirty
warning: creating lock file '/home/king/nuclei/flake.lock'
warning: Git tree '/home/king/nuclei' is dirty
[0/1 built, 35.6 MiB DL] querying nix-shell-env on https://cache.nixos.orgdirenv: ([/nix/store/bkay3lb41czmhka6isvggbdc5lk47yzx-direnv-2.32.3/bin/direnv export bash]) is taking a while to execute. Use CTRL-C to give up.
warning: Git tree '/home/king/nuclei' is dirty
```

Let's go ajouter les fichiers en trop dans le `.gitignore` :

```sh
.idea
.vscode
.devcontainer
v2/vendor
v2/dist
integration_tests/nuclei
integration_tests/integration-test
v2/cmd/nuclei/main
v2/cmd/nuclei/nuclei
v2/cmd/integration-test/nuclei
v2/cmd/functional-test/nuclei_dev
v2/cmd/functional-test/nuclei_main
v2/cmd/functional-test/functional-test
v2/cmd/docgen/docgen
v2/pkg/protocols/common/helpers/deserialization/testdata/Deserialize.class
v2/pkg/protocols/common/helpers/deserialization/testdata/ValueObject.class
v2/pkg/protocols/common/helpers/deserialization/testdata/ValueObject2.ser
*.exe
v2/.gitignore
*.DS_Store
result
.direnv
```

On ajoute le `devShell` et roule bouboule.

## 3 - Build System

Une fois toutes les commandes effectuées, on se rertouve avec un truc nouveau :

```sh
[king@localhost v2]$ task
task: [init:go] go mod download

task: Available tasks for this project:
* build:         Build nuclei
* default:       Display help
* init:          Setup project
[king@localhost v2]$ task init
task: Task "init:go" is up to date
[king@localhost v2]$ task build
task: Task "init:go" is up to date
internal/goarch
internal/unsafeheader
internal/abi
internal/cpu
...
```

Nuclei works. Perfect.

Les `deps` sont des tâches lancées automatiquement avant la tâche principale.

## 4 - Pure

Ajoutons ce qui est demandé :

```sh
goPackage = pkgs.buildGoModule {
  name = "nuclei";
  version = "v2.9.15";
  src = ./v2/.;
  buildInputs = [
    # required for tests
    pkgs.firefox
  ];
  vendorSha256 = "sha256-3buSAVyXQ8epc4kteFCemEBMZxo68a4SpTEo27EV0Fw=";
  ldflags = [
    "-s"
    "-w"
  ];
};
```

On oublie pas le `package.default = goPackage` et on enchaîne.

On bombarde les actions entre :

- `rm ./v2/nuclei`
- `nix build`
- `ls -la | grep result`
- `result/bin/nuclei`
- `...`

Beacucoup de lecture mais pas grand chose à faire.

## 5 - Container

No questions to answer

## 6 - Fmt

On ajoute le bloc de code dans la section let :

```sh
nixFormatterPackages = with pkgs; [
  nixpkgs-fmt
  alejandra
  statix
];
```

Dans les 3 lignes :

- `nixpkgs-fmt` = *nix packages formatter* = Outil de formatage Nix Code pour nixpkgs
- `alejandra` = *Alexandra en grec* = Outil de non-compromission du Nix Code Formatter (un anti-crack en gros)
- `statix` = *gaulois statique* = Lints et Suggestions pour Nix

Ajoutons ce qui est demandé :

```sh
formatter = pkgs.writeShellApplication {
  name = "normalise_nix";
  runtimeInputs = nixFormatterPackages;
  text = ''
    set -o xtrace
    alejandra "$@"
    nixpkgs-fmt "$@"
    statix fix "*.nix"
  '';
};
```

Ainsi que :

```sh
checks = {
  inherit goPackage;
  typos = pkgs.mkShell {
    buildInputs = with pkgs; [ typos ];
    shellHook = ''
      typos .
    '';
  };
  yamllint = pkgs.mkShell {
    buildInputs = with pkgs; [ yamllint ];
    shellHook = ''
      yamllint --strict .
    '';
  };
  reuse = pkgs.mkShell {
    buildInputs = with pkgs; [ reuse ];
    shellHook = ''
      reuse lint
    '';
  };
};
```

Puis le `devshell` :

```sh
devShells.default = pkgs.mkShell {
  inputsFrom = builtins.attrValues self.checks.${system};
  buildInputs = with pkgs;
  [
    go
    gopls
    gotools
    go-tools
    podman
  ]
  ++ nixFormatterPackages;
};
```

Enfin on exécute `nix fmt` suivi de `nix flake check`.

Apparement faut ajouter ce bloc de code pour ce qui est des fautes d'orthographes, mais useless un peu puisque personne n'y fais jamais gaffe :

```sh
lint::typos:
    silent: true
    cmds:
      - |
        nix run nixpkgs#typos -- --write-changes .
  lint:
    silent: true
    desc: Lint jardin
    deps:
      - lint::typos
```

Puis on le lance avec `task lint`.

## Taskfile & nix

No question to answer.

## Pre-commit

On va passer tout les blocs de code supplémentaire à ajouter ou modifier, on refait juste les mêmes commandes pour valider et recommencer (youhou).

## Pipeline

"On ne va pas faire de pipeline car elle nécessite un runner et un outil de CI."

OH BAH SUPER, SUPER!

## FIN
